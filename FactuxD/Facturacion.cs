﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria1;

namespace FactuxD
{
    public partial class Facturacion : Procesos
    {
        public Facturacion()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Facturacion_Load(object sender, EventArgs e)
        {

            try
            {

                string cmd = "Select * from Usuarios where id_usuario=" + VentanaLogin.Codigo;

                DataSet ds;

                ds = Utilidades.Ejecutar(cmd);

                lblLeAtiende.Text = ds.Tables[0].Rows[0]["Nom_usu"].ToString().Trim();
            }
            catch
            {

            }
        }

        private void errorTxtBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void errorTxtBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void errorTxtBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {


            if (cont_fila != 0)
            {


                try
                {

                    string cmd = string.Format("Exec ActualizaFacturas'{0}'", txtCodigoCliente.Text.Trim());

                    DataSet ds = Utilidades.Ejecutar(cmd);

                    string Numfac = ds.Tables[0].Rows[0]["NumFac"].ToString().Trim();

                    foreach(DataGridViewRow Fila in dataGridView1.Rows)
                    {
                        cmd = string.Format("Exec ActualizaDetalles '{0}','{1}','{2}','{3}'", Numfac, Fila.Cells[0].Value.ToString(), Fila.Cells[2].Value.ToString(), Fila.Cells[3].Value.ToString());

                        ds = Utilidades.Ejecutar(cmd);



                    }

                    cmd = "Exec DatosFactura " + Numfac;

                    ds = Utilidades.Ejecutar(cmd);


                    //Ventana Reporte

                    Reporte rp = new Reporte();

                    rp.reportViewer1.LocalReport.DataSources[0].Value = ds.Tables[0];

                    rp.ShowDialog();
                    Nuevo();
                } catch(Exception error)
                {
                    MessageBox.Show("Error: " + error.Message);

                }



            }


        }

        private void button7_Click(object sender, EventArgs e)
        {

            try
            {
                if (string.IsNullOrEmpty(txtCodigoCliente.Text.Trim()) == false)
                {
                    string cmd = String.Format("Select Nom_cli FROM Cliente where id_clientes='{0}'", txtCodigoCliente.Text.Trim());

                    DataSet ds = Utilidades.Ejecutar(cmd);

                    txtCliente.Text = ds.Tables[0].Rows[0]["Nom_cli"].ToString().Trim();
                    txtCodigoProducto.Focus();

                }
            }
            catch(Exception error)
            {
                MessageBox.Show("Ha ocurrido un error:" + error.Message);

            }
          
        }

        public static int cont_fila = 0;
        public static double total;

        private void btnColocar_Click(object sender, EventArgs e)
        {



            if (Utilidades.ValidarFormulario(this, errorProvider1) == true)
            {
                bool existe = false;
                int num_fila = 0;


                if (cont_fila == 0)
                {
                    
                        dataGridView1.Rows.Add(txtCodigoProducto.Text, txtDescripcion.Text, txtPrecio.Text, txtCantidad.Text);

                        double importe = Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[2].Value) * Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[3].Value);

                        dataGridView1.Rows[cont_fila].Cells[4].Value = importe;
                        cont_fila++;
                   
                }
                else
                {

                    foreach(DataGridViewRow Fila in dataGridView1.Rows)
                    {

                        if (Fila.Cells[0].Value.ToString() == txtCodigoProducto.Text)
                        {
                            existe = true;
                            num_fila = Fila.Index;

                        }
                    }

                    if (existe == true)
                    {
                        dataGridView1.Rows[num_fila].Cells[3].Value = (Convert.ToDouble(txtCantidad.Text) + Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[3].Value));
                            double importe = Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[2].Value) * Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[3].Value);


                        dataGridView1.Rows[num_fila].Cells[4].Value = importe;


                    }
                    else
                    {


                        dataGridView1.Rows.Add(txtCodigoProducto.Text, txtDescripcion.Text, txtPrecio.Text, txtCantidad.Text);


                        double importe = Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[2].Value) * Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[3].Value);



                        dataGridView1.Rows[cont_fila].Cells[4].Value = importe;


                        cont_fila++;


                    }


                }

                total = 0;
                foreach (DataGridViewRow Fila in dataGridView1.Rows)
                {
                    total += Convert.ToDouble(Fila.Cells[4].Value);
                   
                }

                lblTotal.Text = "RD$ " + total.ToString();



            }


        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (cont_fila > 0)
            {

                total = total - (Convert.ToDouble(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].Value));

                lblTotal.Text = "RDS " + total.ToString();


                dataGridView1.Rows.RemoveAt(dataGridView1.CurrentRow.Index);



                cont_fila--;

            }



        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            ConsultarClientes ConCli = new ConsultarClientes();
            ConCli.ShowDialog();
            if (ConCli.DialogResult == DialogResult.OK)
            {
                txtCodigoCliente.Text = ConCli.dataGridView1.Rows[ConCli.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtCliente.Text = ConCli.dataGridView1.Rows[ConCli.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();

                txtCodigoProducto.Focus();

            }
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {

            ConsultarProductos ConPro = new ConsultarProductos();
            ConPro.ShowDialog();

            if (ConPro.DialogResult == DialogResult.OK)
            {

                txtCodigoProducto.Text = ConPro.dataGridView1.Rows[ConPro.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtDescripcion.Text = ConPro.dataGridView1.Rows[ConPro.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
                txtPrecio.Text = ConPro.dataGridView1.Rows[ConPro.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
                txtCantidad.Focus();

            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {


            Nuevo();



        }


        public override void Nuevo()
        {
            txtCodigoCliente.Text = "";
            txtCliente.Text = "";
            txtCodigoProducto.Text = "";
            txtDescripcion.Text = "";
            txtPrecio.Text = "";
            txtCantidad.Text = "";
            lblTotal.Text = "RD$ 0";
            dataGridView1.Rows.Clear();
            cont_fila = 0;
            total = 0;
            txtCodigoCliente.Focus();


        }

        private void lblLeAtiende_Click(object sender, EventArgs e)
        {

        }
    }
}
